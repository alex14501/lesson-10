#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string name);
	~Customer();
	double totalSum() ;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set

	//get and set functions

private:
	string _name;
	set<Item> _items;


};
