#include"Customer.h"
#include<map>
#include <string>
#define SIGN_UP 1
#define EXISTING_USER 2
#define GET_MAX_VALUE_CART 3
#define EXIT 4
#define ADD 1
#define REMOVE 2
#define GO_BACK 0
int main()
{

	map<string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};
	int choice = 0;
	std::string name;
	double max = 0;
	std::map<string, Customer>::iterator iter;
	while (choice != EXIT)
	{
		cout << "Welcome to MagshiMart\n1.To sign as customer and buy items\n2.To update existing custome's items\n3.To print the customer who pays the most\n4.To Exit" << endl;
		cin >> choice;
		switch (choice)
		{
		case SIGN_UP:
			cout << "Please enter your name:" << std::endl;
			cin >> name;
			iter = abcCustomers.find(name);
			if (iter != abcCustomers.end())
			{
				std::cout << "Name already exists in the system... \ngoing back to menu...";
			}
			else
			{
				Customer customer(name);
				while (choice != GO_BACK)
				{
					cout << "Which item you want to buy" << endl;
					for (int i = 0; i < 10; i++)
					{
						cout << i + 1 << ". " << itemList[i].getName() << " " << "price: " << itemList[i].getPrice() << endl;
					}
					cin >> choice;
					if (choice != GO_BACK)
					{
						customer.addItem(itemList[choice - 1]);
						cout << "Items was succsesfuly bought" << endl;
					}
					else
					{
						if (choice > 10||choice<0)
						{
							cout << "Sorry but that item does not exist." << endl;
						}
					}
				}
				pair<string, Customer> pair(name, customer);
				abcCustomers.insert(pair);
			}
			break;
		case EXISTING_USER:
				cout << "Please enter you'r name" << endl;
				cin >> name;
				iter = abcCustomers.find(name);
				if (iter == abcCustomers.end())
				{
					std::cout << "This name was not found in the system... \nGoing back to main..." << endl;
				}
				else
				{
					cout << "Please enter a choice\n1.Add item\n2.Delete item\n0.Back to menu" << endl;
					cin >> choice;
					switch (choice)
					{
					case ADD:
						cout << "Which item do you want to buy" << endl;
						for (int i = 0; i < 10; i++)
						{
							cout << i + 1 << ". " << itemList[i].getName() << " " << "price: " << itemList[i].getPrice() << endl;
						}
						cin >> choice;
						if (choice != GO_BACK)
						{
							abcCustomers.find(name)->second.addItem(itemList[choice - 1]);
						}
						else
						{
							if (choice > 10 || choice < 0)
							{
								cout << "Sorry but that item does not exist in our System...\nGoing back to the Menu"<<endl;
							}
						}
						break;
					case REMOVE:
						while (choice != GO_BACK)
						{
							cout << "which item you want to delete" << endl;;
							for (int i = 0; i < 10; i++)
							{
								cout << i + 1 << ". " << itemList[0].getName() << " " << "price: " << itemList[i].getPrice() << endl;
							}
							cin >> choice;
							if (choice != GO_BACK)
							{
								abcCustomers.find(name)->second.removeItem(itemList[choice - 1]);
							}
						}
						break;
					}
				}
			
			break;
		case GET_MAX_VALUE_CART:
			for (iter = abcCustomers.begin(); iter != abcCustomers.end(); iter++)
			{
				if ((*iter).second.totalSum() > max)
				{
					max = (*iter).second.totalSum();
					name = (*iter).first;
				}
			}
			cout <<"The customer with the most Valueble cart is: "<< name << endl;
		default:
			break;
		}
	}
	/*iter = abcCustomers.begin();
	for(iter = abcCustomers.begin();iter != abcCustomers.end();iter++)
	{ 
		(*iter).second.~Customer();
	}*/

	return 0;
}