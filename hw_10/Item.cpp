#include "Item.h"
Item::Item(string name, string serialNumber, double price)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = price;
	this->_count = 1;
}
Item::~Item()
{
}
void Item::setCount(int count)
{
	this->_count = count;
}
string Item::getName() const
{
	return this->_name;
}
void Item::decCount() 
{
	this->_count--;
}
void Item::addCount() 
{
	this->_count++;
}
string Item::getSerialNumber() const
{
	return this->_serialNumber;
}
int Item::getCount() const
{
	return this->_count;
}
double Item::getPrice()
{
	return this->_unitPrice;
}
double Item::totalPrice() const //returns _count*_unitPrice
{
	return this->_count*this->_unitPrice;
}
bool Item::operator <(const Item& other) const //compares the _serialNumber of those items.
{
	return (this->_serialNumber < other.getSerialNumber());
}
bool Item::operator >(const Item& other) const //compares the _serialNumber of those items.
{
	return this->_serialNumber > other.getSerialNumber();
}
bool Item::operator ==(const Item& other) const//compares the _serialNumber of those items.
{
	return this->_serialNumber == other.getSerialNumber();
}