#include "Customer.h"
#include "Item.h"
#include <set>
#include <map>
#include <string>
#include <iostream>
Customer::Customer(string name)
{
	this->_name = name;
}
Customer::~Customer()
{
	cout << "d'tor";
	system("pause");
}
double Customer::totalSum() //returns the total sum for payment
{
	double sum = 0;
	std::set<Item>::iterator iter;
	for (iter = this->_items.begin(); iter!=this->_items.end(); iter++)
	{
		sum += (*iter).totalPrice();
	}
	return sum;
}
void Customer::addItem(Item item)//add item to the set
{
	bool flag = true;
	std::set<Item>::iterator iter;
	iter = this->_items.find(item);
	if (iter!=this->_items.end())
	{
		item.setCount((*iter).getCount() + 1);
		flag = false;
		this->_items.erase(item);
		this->_items.insert(item);
	}
	else
	{
		this->_items.insert(item);
	}
}
void Customer::removeItem(Item item)//remove item from the set
{
	bool flag = true;
	std::set<Item>::iterator iter;
	iter = this->_items.find(item);
	if (iter != this->_items.end())
	{
		if ((*iter).getCount() == 1)
		{
			this->_items.erase(item);
		}
		else
		{
			item.setCount((*iter).getCount() - 1);
			this->_items.erase(item);
			this->_items.insert(item);
		}
	}
	else
	{
		cout << "item doesnt exist" <<  endl;
	}

}